const ENVIRONMENTS = {
    development: {
        API_URL: 'http://localhost:3333',
    },
    production: {
        API_URL: 'http://teste.com',
    },
};

export { ENVIRONMENTS };
